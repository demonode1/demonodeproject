const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

const routerUser = require('./routes/user')
app.use('/user', routerUser)

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})
