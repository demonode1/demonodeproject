const express = require('express')
const router = express.Router()

router.post('/signup', (request, response) => {
    console.log('signup called')
    response.send('signed up')
})


router.post('/signin', (request, response) => {
    console.log('signin called')
    response.send('signed in')
})

module.exports = router
